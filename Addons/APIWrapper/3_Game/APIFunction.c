class APIFunction
{ 
	const float MAX_TIME 			= 86400;
	
	protected Class 				m_FunctionClass;
	protected string 				m_FunctionName;
	protected int					m_Delay				= 0; // In seconds
	protected float					m_Timer				= 0; // In seconds
	protected float					m_LastTimer			= 0; // In seconds
	protected APIFunctionTime 		m_TimerMode			= 0;
	protected bool					m_IsRepeat			= false;
	protected bool					m_IsBreak			= false;
	protected Param					m_ParamData			= NULL;
	
	// Repeat
	void SetRepeat(bool enable)		
	{
		m_IsRepeat 	= enable;
		m_IsBreak	= false;
	}
	
	void SetParam(ref Param param)	m_ParamData = param;
	void SetBreak(bool isBreak) m_IsBreak = ;
	
	// Read variables
	int 				GetDelay()			return m_Delay;
	float 				GetTimer()			return m_Timer;
	float 				GetLastTimer()		return m_LastTimer;
	APIFunctionTime 	GetTimeType() 		return m_TimerMode;
	bool 				IsRepeat()			return m_IsRepeat;
	bool 				IsBreak()			return m_IsBreak;
	
	void APIFunction(Class classFn = NULL, string nameFn = "")
	{
		m_FunctionClass = classFn;
		m_FunctionName	= nameFn;
	}
	
	void SetGameTimer(int hour, int minute, int seconds = 0)
	{
		m_TimerMode = APIFunctionTime.GAME_TIME;
		m_Timer = string.Format("%1.%2", hour, minute * 166.65 + seconds * 2.7775).ToFloat();
		SetBreak(false);
	}
	
	void SetLocalTimer(int hour, int minute, int seconds = 0)
	{
		m_TimerMode = APIFunctionTime.LOCAL_TIME;
		m_Timer = APIMath.Mod(hour * 60 * 60 + minute * 60 + seconds, MAX_TIME);
		SetBreak(false);
	}
	
	void SetDelayTimer(int seconds)
	{
		m_TimerMode = APIFunctionTime.DELAY;
		m_Delay = seconds;
		m_Timer = GetGame().GetTime() * 0.001 + seconds;
		SetBreak(false);
	}
	
	void SetTimer(int mathTime)
	{
		switch(m_TimerMode)
		{
			case APIFunctionTime.GAME_TIME:
			{
				m_Timer = APIMath.Mod(m_Timer + mathTime, 24);
				break;
			}
			case APIFunctionTime.LOCAL_TIME:
			{
				m_Timer = APIMath.Mod(m_Timer + mathTime, MAX_TIME);
				break;
			}
			default:
			{
				m_Timer = m_Timer + mathTime;
				break;
			}
		}
		SetBreak(false);
	}
	
	bool IsValidTime()
	{
		if(IsBreak()) return false;
		switch(m_TimerMode)
		{
			case APIFunctionTime.GAME_TIME: return GetGame().GetDayTime() >= m_Timer;
			case APIFunctionTime.LOCAL_TIME:
			{
				int h; int m; int s;
				GetHourMinuteSecond(h, m, s);
				return h * 60 * 60 + m * 60 + s >= m_Timer;;
			}
			default: return (int)(GetGame().GetTime() * 0.001) >= m_Timer;
		}
		return false;
	}
	
	void Invoke(ref Param param = NULL) 
	{
		if(param) m_ParamData = param;
		GetGame().GameScript.CallFunctionParams(m_FunctionClass, m_FunctionName, NULL, m_ParamData);
		m_LastTimer = m_Timer;
	}
	
	bool InvokeTime(ref Param param = NULL)
	{
		if(!IsValidTime()) return false;
		if(!IsRepeat()) SetBreak(true);
		
		// Exec
		Invoke(param);
		
		// Set Delay
		if(m_TimerMode == APIFunctionTime.DELAY) SetTimer(m_Delay);
		return true;
	}
}

enum APIFunctionTime
{
	DELAY,
	GAME_TIME,
	LOCAL_TIME
}