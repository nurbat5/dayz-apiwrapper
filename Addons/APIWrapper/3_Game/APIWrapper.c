class APIWrapper : APIConnect
{
	ref event MyEvent = new event;
	void OnCreate()
	{
		MyEvent.Attach(APIWrapper);
		MyEvent.CallQueue(this, "LaterMinute", NULL, 60);
	}
	
	void LaterMinute()
	{
		Logger.Full(this, "1 Minute Later");
	}
}