typedef APILogger Logger;
class APILogger
{
	static void Full(Class errorClass, string errorType, string errorText = "")
		Log(errorClass, "", errorType, errorText, 6);
	
	static void Success(Class errorClass, string errorType, string errorText = "")
		Log(errorClass, "", errorType, errorText, 5);
	
	static void Mod(Class errorClass, string errorType, string errorText = "")
		Log(errorClass, "", errorType, errorText, 4);
	
	static void Info(Class errorClass, string errorType, string errorText = "")
		Log(errorClass, "", errorType, errorText, 3);
	
	static void Warning(Class errorClass, string errorType, string errorText = "")
		Log(errorClass, "", errorType, errorText, 2);
	
	static void Danger(Class errorClass, string errorType, string errorText = "")
		Log(errorClass, "", errorType, errorText, 1);
	
	static void Error(Class errorClass, string errorType, string errorText = "")
		Log(errorClass, "", errorType, errorText, 0);
	
	// [Date  Time]			[Class]		[Prefix] ->	ErrorType ( errorText )
	//[29.08.2019 12:46:58]	[APIManager][FULL] 	-> 	NO_FIND ( DayZ )
	static void Log(Class errorClass, string prefix = "", string errorType = "", string errorText = "", int debugLevel = -1)
	{
		//if(APISettings.DebugLevel() >= debugLevel)
		//{
			string resultText = "";
			if(errorClass 	!= NULL) 	resultText = resultText + string.Format("[%1]", errorClass.ClassName());
			if(prefix 		!= "") 		resultText = resultText + string.Format("[%1]", prefix);
			if(errorType 	!= "") 		resultText = resultText + string.Format(" -> %1", errorType);
			if(errorText 	!= "") 		resultText = resultText + string.Format(" ( %1 )", errorText);
			Write(resultText);
		//}
	}
	
	static void Write(string message)
	{
		//if(APISettings.DefaultLog())
		//{
			Print(string.Format("[%1]%2", API.GetDate(), message));
			return;
		//}
		//Print(string.Format("[%1]%2", APIWrapper.GetDate(), message));
	}
}