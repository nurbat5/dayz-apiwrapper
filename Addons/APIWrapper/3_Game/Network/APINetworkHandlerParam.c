class APINetworkHandlerParam : APINetworkMessageDelegate
{
	APINetworkMessage ReadRPC(APINetworkMessage msg)
	{
		Param1<APINetworkMessage> paramMessage;
		if(msg.GetContext().Read(paramMessage)) return paramMessage.param1;
		return NULL;
	}
}