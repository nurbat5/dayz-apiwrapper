class APIDataStorage : APINetworkMessageWriter {} // Alias
class APINetworkMessageWriter
{
	autoptr map<string, string> 		contentptrData		= new map<string, string>();
	autoptr map<string, ref Class> 		contentPtrClass		= new map<string, ref Class>();
	autoptr map<string, ref Managed> 	contentPtrManaged	= new map<string, ref Managed>();
	
	// Base Write
	void SetItem(string key, string value) 		contentptrData[key] = value;
	void SetItem(string key, ref Class value) 	contentPtrClass[key] = value;
	void SetItem(string key, ref Managed value) contentPtrManaged[key] = value;

	// Base types
	void SetItem(string key, int value) 		contentptrData[key] = value.ToString();
	void SetItem(string key, bool value) 		contentptrData[key] = value.ToString();
	void SetItem(string key, float value) 		contentptrData[key] = value.ToString();
	void SetItem(string key, vector value) 		contentptrData[key] = value.ToString();
	void SetItem(string key, typename value) 	contentptrData[key] = value.ToString();
	
	// Base Read
	bool GetItem(string key, out string value) 		return contentptrData.Find(key, value);
	bool GetItem(string key, out ref Class value) 	return contentPtrClass.Find(key, value);
	bool GetItem(string key, out ref Managed value) return contentPtrManaged.Find(key, value);
	
	// Base types read
	bool GetItem(string key, out int value) 
	{
		string text; 
		if(contentptrData.Find(key, text))
		{
			value = text.ToInt();
			return true;
		}
		return false;
	}
	
	bool GetItem(string key, out bool value)
	{
		string text; 
		if(contentptrData.Find(key, text))
		{
			value = text.ToInt();
			return true;
		}
		return false;
	}
	
	bool GetItem(string key, out float value)
	{
		string text; 
		if(contentptrData.Find(key, text))
		{
			value = text.ToFloat();
			return true;
		}
		return false;
	}
	
	bool GetItem(string key, out vector value)
	{
		string text; 
		if(contentptrData.Find(key, text))
		{
			value = text.ToVector();
			return true;
		}
		return false;
	}
	
	bool GetItem(string key, out typename value)
	{
		string text; 
		if(contentptrData.Find(key, text))
		{
			value = text.ToType();
			return true;
		}
		return false;
	}
	
	// UNSAFE // DANGER // UNSAFE
	string 		UnsafeGetString(string key) 	return contentptrData.Get(key);
	int 		UnsafeGetInt(string key) 		return contentptrData.Get(key).ToInt();
	bool 		UnsafeGetBool(string key) 		return contentptrData.Get(key).ToInt();
	float 		UnsafeGetFloat(string key) 		return contentptrData.Get(key).ToFloat();
	vector 		UnsafeGetVector(string key) 	return contentptrData.Get(key).ToVector();
	typename 	UnsafeGetType(string key) 		return contentptrData.Get(key).ToType();
	ref Class 	UnsafeGetClass(string key) 		return contentPtrClass.Get(key);
	ref Managed UnsafeGetManaged(string key) 	return contentPtrManaged.Get(key);
	
	
	void Clear()
	{
		contentptrData.Clear();
		contentPtrClass.Clear();
		contentPtrManaged.Clear();
	}
}