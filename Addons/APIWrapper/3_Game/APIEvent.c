typedef APIEvent event;
class APIEvent : APIModule
{
	protected bool								m_IsEventStart;
	protected int 								m_NextTimer;
	protected int								m_StepTimer;

	protected autoptr array<ref APIFunction> 	m_Functions 			= new array<ref APIFunction>();
	protected autoptr array<ref APIFunction> 	m_InvokeFunc 			= new array<ref APIFunction>();
	
	autoptr array<ref APIFunction> 				GetFunctions() 			return m_Functions;
	autoptr array<ref APIFunction> 				GetInvokeFunctions() 	return m_InvokeFunc;
	
	void APIEvent(int stepTime = 1, int nextTime = 60)
	{ 
		m_StepTimer = stepTime;
		m_NextTimer = nextTime;
	}
	
	protected int GameTime() return GetGame().GetTime() * 0.001;

	int CallTimeQueue(Class classFn, string nameFn, int hour, int minute, Param param = NULL, bool repeat = false) 
	{
		ref APIFunction fn = new APIFunction(classFn, nameFn);
		fn.SetParam(param);
		fn.SetRepeat(repeat);
		fn.SetLocalTimer(hour, minute);
		return m_Functions.Insert(fn);
	}
		
	int CallGameQueue(Class classFn, string nameFn, int hour, int minute, Param param = NULL, bool repeat = false) 
	{
		ref APIFunction fn = new APIFunction(classFn, nameFn);
		fn.SetParam(param);
		fn.SetRepeat(repeat);
		fn.SetGameTimer(hour, minute);
		return m_Functions.Insert(fn);
	}
	
	int CallQueue(Class classFn, string nameFn, Param param = NULL, int delay = 0, bool repeat = false)
	{
		ref APIFunction fn = new APIFunction(classFn, nameFn);
		fn.SetParam(param);
		fn.SetRepeat(repeat);
		fn.SetDelayTimer(delay);
		return m_Functions.Insert(fn);
	}
	
	// Invoke
	void InvokeTime(Param param = NULL)
	{
		for(int i = 0; i < GetInvokeFunctions().Count(); i++)
		{
			GetInvokeFunctions()[i].InvokeTime(param);
		}
	}
	
	void Invoke(Param param = NULL)
	{
		for(int i = 0; i < GetInvokeFunctions().Count(); i++)
		{
			GetInvokeFunctions()[i].Invoke(param);
		}
	}
	
	ref APIFunction AddInvoke(Class classFn, string nameFn)
	{
		ref APIFunction fn = new APIFunction(classFn, nameFn);
		GetInvokeFunctions().Insert(fn);
		return fn;
	}
	
	void OnInit() { m_IsEventStart = true; } // Needed. Function bohemia call crash.. Fix.
	void OnUpdate(float time)
	{
		if(!m_IsEventStart) return; // Needed. Function bohemia call crash.. Fix.
		if(GameTime() <= m_NextTimer) return;
		
		APIFunction fn;
		for(int i = 0; i < GetFunctions().Count(); i++)
		{
			// Clear cache array events
			fn = GetFunctions()[i];
			if(fn.IsBreak()) 
			{
				GetFunctions().Remove(i);
				delete fn;
				continue;
			}
			
			fn.InvokeTime();
		}
		
		m_NextTimer = m_NextTimer + m_StepTimer;
	}
}
