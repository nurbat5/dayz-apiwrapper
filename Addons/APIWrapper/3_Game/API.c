class API
{
	static string GetGameTime()
	{
		ref array<string> arrayTime = new array<string>();
		GetGame().GetDayTime().ToString().Split(".", arrayTime);	
		return string.Format("%1:%2", arrayTime[0], arrayTime[1]);
	}
	
	static string GetLocalTime()
	{
		ref array<string> arrayTime = new array<string>();
		int hour; int minute; int seconds;
		GetHourMinuteSecond(hour, minute, seconds);
		return string.Format("%1:%2:%3", arrayTime[0], arrayTime[1], arrayTime[2]);
	}
	
	static string GetDate(bool fileFriendly = false) 
	{
		int year, month, day, hour, minute, second;

		GetYearMonthDay(year, month, day);
		GetHourMinuteSecond(hour, minute, second);

		string date = day.ToStringLen(2) + "." + month.ToStringLen(2) + "." + year.ToStringLen(4) + " " + hour.ToStringLen(2) + ":" + minute.ToStringLen(2) + ":" + second.ToStringLen(2);
		if (fileFriendly) 
		{
			date.Replace(" ", "_");
			date.Replace(".", "-");
			date.Replace(":", "-");
		}
		return date;
	}
	
	static bool IsDebug() return GetGame().IsDebug();
	static bool IsMultiplayer() return GetGame().IsMultiplayer();
	
	static bool IsServer()
	{
		#ifdef SERVER
		return true;
		#else
		return false;
		#endif
	}
	
	static bool IsClient()
	{
		#ifdef SERVER
		return false;
		#else
		return true;
		#endif
	}
}