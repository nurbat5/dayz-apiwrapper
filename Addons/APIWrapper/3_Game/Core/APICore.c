class APICore : APIModule
{
	protected 	ref array 	<ref APIModule>	m_CArray;
	protected  	ref map	 	<typename, int> m_CMap;
	
	ref array 	<ref APIModule> 			GetModuleArray() 	return m_CArray;
	ref map	 	<typename, int> 			GetModuleMap() 		return m_CMap;
	
	void APICore()
	{
		m_CArray 	= new array 	<ref APIModule>;
		m_CMap 		= new map 		<typename, int>;
	}
	
	void ~APICore()
	{
		APIModule module;
		for (int i = 0; i < GetModuleArray().Count(); ++i)
		{
			module = GetModuleArray().Get(i);
			if ( module != NULL )
			{
				module.OnDestroy();
				delete module;
			}
		}
		
		// Log
		Logger.Full(this, "DESTROY");
	}
	
	private bool InitModule(string moduleName)
	{
		typename moduleType = moduleName.ToType();
		
		if(!moduleType.IsInherited( APIModule )) 
		{
			if(moduleType.ToString() == "") 	{ Logger.Full(this, moduleName, "NO FIND"); }
			else 								{ Logger.Full(this, moduleName, " NO LOAD"); }
			return false;
		}

		// Exist Module
		if ( GetModuleByType(moduleType) ) Unregister(moduleName);

		// Instance Wrapper
		APIModule module = APIModule.Cast(moduleType.Spawn());
		if ( module )
		{
			// Log
			Logger.Success(this, "LOADED", moduleName);
			
			// Array
			int index = GetModuleArray().Insert(module);
			
			// Map
			GetModuleMap().Set(moduleType, index);
			
			// Instance
			module.OnCreate();
			return true;
		}
		
		// Log
		Logger.Error(this, "FAIL LOAD", moduleName);
		return false;
	}
	
	// Register main wrapper
	protected bool Register( string moduleName, bool onClient = true, bool onServer = true, bool OnDebug = false)
	{
		if ( !onClient && API.IsMultiplayer() && API.IsClient() ) 	{ Logger.Full(this, "Register", "Client Module, ERROR: " 	+ moduleName); return false; }
		if ( !onServer && API.IsMultiplayer() && API.IsServer() ) 	{ Logger.Full(this, "Register", "Server Module, ERROR: " 	+ moduleName); return false; }	
		if ( !OnDebug && API.IsDebug() ) 							{ Logger.Full(this, "Register", "Debug Module, ERROR: " 	+ moduleName); return false; }
		InitModule(moduleName);
		return true;
	}
	
	protected bool Unregister(string moduleName)
	{
		typename moduleType = moduleName.ToType();
		int index;
		
		if ( GetModuleMap().Find(moduleType, index))
		{
			// Get
			APIModule module = GetModuleArray()[index];
			
			module.OnDestroy();
			delete module;
			
			// Remove
			GetModuleArray().Remove(index);
			GetModuleMap().Remove(moduleType);
			
			// Log
			Logger.Success(this, "UNREGISTER WRAPPER", moduleName);
			return true;
		}		
		Logger.Error(this, "UNREGISTER WRAPPER", moduleName);
		return false;
	}

	// Attach Additation classes
	int Attach(typename moduleType, APIModule module)
	{
		APIConnect moduleConnect = APIConnect.Cast(GetModuleByType(moduleType));
		if(moduleConnect != NULL)
		{
			int index = moduleConnect.GetModuleArray().Find(module);
			if(index >= 0)
			{
				Logger.Error(this, "Attach", "Module already exist");
				return -1;
			}
			return moduleConnect.GetModuleArray().Insert(module);
		}
		return -1;
	}
	
	bool Detach(typename moduleType, int index)
	{
		APIConnect moduleConnect = APIConnect.Cast(GetModuleByType(moduleType));
		if(moduleConnect != NULL)
		{
			if(!moduleConnect.GetModuleArray().IsValidIndex(index)) return false;
			moduleConnect.GetModuleArray().Remove(index);
			return true;
		}
		return false;
	}
	
	APIModule GetModuleByType( typename moduleType )
	{
		int index;
		if (GetModuleMap().Find(moduleType, index)) return GetModuleArray().Get(index);
		return NULL;
	}
}