class APICoreManager : APICore
{
	void InitAddons()
	{
		//----------------------------------------------------------------------------------
		//				Module Class Name 							Client	Server	Debug
		//----------------------------------------------------------------------------------
		for(int i = 0; i < ModLoader.GetMods().Count(); i++)
		{
			Register(ModLoader.GetMods()[i].GetModName(),			true, 	true,	true);
		}
		Register("APIWrapper",										true, 	true,	true);
	}
	
	static void SendCommandToConnect(typename moduleType, string moduleFunction, ref Param param = NULL)
	{
		if(!GetInstance()) return;
		
		APIConnect moduleConnect = APIConnect.Cast(GetInstance().GetModuleByType(moduleType));
		if(moduleConnect)
		{
			APIModule module;
			for(int i = 0; i < moduleConnect.GetModuleArray().Count(); i++)
			{
				module = moduleConnect.GetModuleArray()[i];
				if(module)
				{
					GetGame().GameScript.CallFunctionParams(module, moduleFunction, NULL, param);
					return;
				}
				Logger.Danger(GetInstance(), "SendCommandToConnect", "NULL CLASS: CRASH !! ModuleName: " + moduleType.ToString() + " ClassName: " + module.ToString());
			}
		}
	}
	
	static void SendCommand(string moduleFunction, ref Param param = NULL)
	{
		if(!GetInstance()) return;
		
		for(int i = 0; i < GetInstance().GetModuleMap().Count(); i++)
		{
			SendCommandToConnect(GetInstance().GetModuleMap().GetKey(i), moduleFunction, param);	
		}
	}
	
	// Instance
	private static autoptr APICoreManager m_Instance;
	static APICoreManager GetInstance() return m_Instance;
	static void CreateInstance()
	{
		if(!m_Instance) 
		{
			m_Instance = new APICoreManager;
			m_Instance.InitAddons();
		}
	}
	static void DeleteInstance()
	{	
		if (m_Instance)
		{
			delete m_Instance;
			m_Instance = NULL;
		}
	}
}
