class APIMath
{
	static int Div(float a, float b)
	{
		int r = 0;
		float q = a / b;
		if(q > 0) r = Math.Floor(q);
		if(q < 0) r = Math.Ceil(q);
		return r;
	}
	
	static float Mod(float a, float b) return a - Div(a, b) * b;
}