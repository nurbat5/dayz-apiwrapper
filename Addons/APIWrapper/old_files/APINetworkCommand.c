class APINetworkCommand : APIWrapperModule
{
	protected ref ScriptRPC 		m_NetworkCommand_RPC;
	protected ref APIWrapperData 	m_NetworkCommand_WrapperData;
	protected ref array<ref Param> 	m_NetworkCommand_StorageParam; // Param
	protected bool 					m_NetworkCommand_Guaranteed;
	
	ref ScriptRPC 			NetworkCommandRPC() 			return m_NetworkCommand_RPC;
	ref APIWrapperData 		NetworkCommandData()	 		return m_NetworkCommand_WrapperData;
	ref array<ref Param> 	NetworkCommandParam() 			return m_NetworkCommand_StorageParam; // Param
	bool 					NetworkCommandTCP() 			return m_NetworkCommand_Guaranteed;
	void 					NetworkCommandTCP(bool enable)  m_NetworkCommand_Guaranteed = enable;
	
	void APINetworkSend()
	{
		m_NetworkCommand_RPC 				= new ScriptRPC();
		m_NetworkCommand_WrapperData		= new APIWrapperData();
		m_NetworkCommand_StorageParam 		= new array<ref Param>(); // Param
		m_NetworkCommand_Guaranteed			= true;
	}
	
	void NetworkCommandReady(string networkFunc = "", APINetworkCall networkCall = APINetworkCall.ServerAndClient)
	{
		NetworkCommandData().Write("NETWORK:Name", 			ClassName());
		NetworkCommandData().Write("NETWORK:Func", 			networkFunc);
		NetworkCommandData().Write("NETWORK:Execution", 	networkCall);
		NetworkCommandData().Write("NETWORK:Guaranteed", 	NetworkCommandTCP());
		NetworkCommandData().Write("NETWORK:From", 			(int)API.IsServer());
		NetworkCommandData().Write("NETWORK:Time", 			GetGame().GetTime());
		
		// Packet
		NetworkCommandRPC().Reset();
		NetworkCommandRPC().Write(NULL);
		NetworkCommandRPC().Write(NetworkCommandData());
	}

	// =========================== NetworkCommandParam ===========================	
	void NetworkCommandSendParam(string networkName, ref array<ref Param> params, Object target = NULL, PlayerIdentity identity = NULL)
	{		
		params.InsertAt(new Param1<ref APIWrapperData>(NetworkCommandData()), 0);
		
		// Send
		//Logger.Full(this, string.Format("%1 SendNetworkParam_Array -> '%2'", ClassName(), networkName), ErrorTypes.RPC_SEND_PACKET);
		GetGame().RPC(target, networkName.Hash(), params, NetworkCommandTCP(), identity);
	}
	
	void NetworkCommandSendParam(string networkName, Object target = NULL, PlayerIdentity identity = NULL)
	{	
		NetworkCommandParam().InsertAt(new Param1<ref APIWrapperData>(NetworkCommandData()), 0);
		
		// Send
		//Logger.Full(this, string.Format("%1 NetworkCommandSendParam -> '%2'", ClassName(), networkName), ErrorTypes.RPC_SEND_PACKET);
		GetGame().RPC(target, networkName.Hash(), NetworkCommandParam(), NetworkCommandTCP(), identity);
		
		// Clear
		NetworkCommandParam().Clear();
	}
	
	// =========================== NetworkCommandData ===========================
	void NetworkCommandSendPacket(string networkName, Object target = NULL, PlayerIdentity identity = NULL)
	{
		// Send
		//Logger.Full(this, string.Format("%1 NetworkCommandSendPacket -> '%2'", ClassName(), networkName), ErrorTypes.RPC_SEND_PACKET);
		NetworkCommandRPC().Send(target, networkName.Hash(), NetworkCommandTCP(), identity);
		
		// Clear
		NetworkCommandRPC().Reset();
		NetworkCommandData().Clear();
	}
}