class APINetworkPacket : APINetworkCommand
{
	// ======================== OnRPC Packet ============================
	protected ParamsReadContext m_NetworkPacket_Context;
	protected PlayerIdentity 	m_NetworkPacket_Sender;
	protected Object 			m_NetworkPacket_Target;
	protected int 				m_NetworkPacket_ID;
	
	ParamsReadContext 			NetworkPacketContext()	return m_NetworkPacket_Context;
	PlayerIdentity 				NetworkPacketSender() 	return m_NetworkPacket_Sender;
	Object 						NetworkPacketTarget() 	return m_NetworkPacket_Target;
	int 						NetworkPacketID() 		return m_NetworkPacket_ID;
	
	// ======================== OnNetwork Packet ========================
	protected string 			m_NetworkPacket_Name;
	protected string 			m_NetworkPacket_Func;
	protected bool 				m_NetworkPacket_Guaranteed;
	protected bool 				m_NetworkPacket_ExistData;
	protected APIWrapperData	m_NetworkPacket_WrapperData;
	protected APINetworkCall	m_NetworkPacket_Execution;
	protected APINetworkCall	m_NetworkPacket_From;
	protected int				m_NetworkPacket_Time;
	
	string 						NetworkPacketName() 	return m_NetworkPacket_Name;
	string 						NetworkPacketFunc() 	return m_NetworkPacket_Func;
	bool 						NetworkPacketTCP() 		return m_NetworkPacket_Guaranteed;
	bool		 				NetworkPacketIsExist() 	return m_NetworkPacket_ExistData;
	APIWrapperData 				NetworkPacketData() 	return m_NetworkPacket_WrapperData;
	APINetworkCall 				NetworkPacketCall() 	return m_NetworkPacket_Execution;
	APINetworkCall 				NetworkPacketFrom() 	return m_NetworkPacket_From;
	int			 				NetworkPacketTime() 	return m_NetworkPacket_Time;

	
	void ClearNetworkPacket()
	{
		m_NetworkPacket_Context 		= NULL;
		m_NetworkPacket_Sender 			= NULL;
		m_NetworkPacket_Target 			= NULL;
		m_NetworkPacket_ID 				= 0;
		
		m_NetworkPacket_Name 			= "";
		m_NetworkPacket_Func 			= "";
		m_NetworkPacket_Guaranteed 		= true;
		m_NetworkPacket_ExistData 		= false;
		m_NetworkPacket_WrapperData 	= NULL;
		m_NetworkPacket_Execution 		= APINetworkCall.ServerAndClient;
		m_NetworkPacket_From 			= APINetworkCall.ServerAndClient;
		m_NetworkPacket_Time 			= 0;
	}
}
