class APINetwork : APINetworkPacket
{
	protected Class 			m_Network_Class;
	protected string  			m_Network_Name;
		
	Class 	GetNetworkClass() 	return m_Network_Class;
	string 	GetNetworkName() 	return m_Network_Name;
	int 	GetNetworkID() 		return m_Network_Name.Hash();
		
	void SetNetworkClass(Class networkClass = NULL)
	{
		m_Network_Class = this;
		if(networkClass) m_Network_Class = networkClass;
	}
	
	void SetNetworkName(string networkName = "")
	{
		m_Network_Name = m_Network_Class.ClassName();
		if(networkName != "") m_Network_Name = networkName;
	}

	void APINetwork(Class networkClass = NULL, string networkName = "")
	{
		SetNetworkClass(networkClass);
		SetNetworkName(networkName);
	}
	
	void OnNetwork( APINetworkPacket networkPacket ) {}
	
	void OnRPC( PlayerIdentity sender, Object target, int id, ParamsReadContext ctx )
	{
		m_NetworkPacket_Context = ctx;
		m_NetworkPacket_Sender	= sender;
		m_NetworkPacket_Target	= target;
		m_NetworkPacket_ID		= id;
		
		Print(" " + m_NetworkPacket_ID);
		if(NetworkPacketID() == GetNetworkID())
		{
			Print("GET");
			// ================= GET PARAM =================
			autoptr Param1<APIWrapperData> paramData;
			if(NetworkPacketContext().Read(paramData)) 
			{
				m_NetworkPacket_WrapperData = paramData.param1;
			}
			else if(!NetworkPacketContext().Read(m_NetworkPacket_WrapperData))
			{
				Logger.Log(this, "ERROR", "FAIL READ", NetworkPacketID().ToString());
				return;
			}
			
			// ================= PARSE DATA =================
			m_NetworkPacket_ExistData = true;
			if(NetworkPacketIsExist())
			{
				if(!NetworkPacketData().Read("NETWORK:Name", 		m_NetworkPacket_Name)) {}
				if(!NetworkPacketData().Read("NETWORK:Func", 		m_NetworkPacket_Func)) {}
				if(!NetworkPacketData().Read("NETWORK:Guaranteed", 	m_NetworkPacket_Guaranteed)) {}
				if(!NetworkPacketData().Read("NETWORK:Execution", 	m_NetworkPacket_Execution)) {}
				if(!NetworkPacketData().Read("NETWORK:From", 		m_NetworkPacket_From)) {}
				if(!NetworkPacketData().Read("NETWORK:Time", 		m_NetworkPacket_Time)) {}
				
				if(!CheckNetworkCall()) 
				{
					Logger.Full(this, "NO VALID", NetworkPacketID().ToString());
					ClearNetworkPacket();
					return;
				}
					
				OnNetwork( this );
				if(NetworkPacketFunc() != "")
				{
					GetGame().GameScript.Call(this, NetworkPacketFunc(), (APINetworkPacket)this);
				}
			}// else Logger.Error(this, GetPacketID().ToString(), ErrorTypes.NETWORK_NO_PACKET);
	
		}
		ClearNetworkPacket();
	}

	bool CheckNetworkCall()
	{
		switch(NetworkPacketCall())
		{
			case APINetworkCall.Server: 
			{
				if(API.IsServer()) return true;
				
				// Log
				//Logger.Full(this, string.Format("%1::%2 -> Server = %3", GetPacketID(), GetNetworkName(), API.IsServer()), ErrorTypes.NO_VALID);
				return false;
			}
			case APINetworkCall.ServerAndClient: 
			{
				m_NetworkPacket_Execution = API.IsServer();
				return true;
			}
			case APINetworkCall.Client:
			{
				if(API.IsClient()) return true;
						
				// Log
				//Logger.Info(this, string.Format("%1::%2 -> Client = %3", GetPacketID(), GetNetworkName(), API.IsClient()), ErrorTypes.NO_VALID);
				return false;
			}
		}
		return false;
	}
}
